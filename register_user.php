<?php
    session_start();
    if (!isset($_SESSION['IS_CONNECTED'])) {
        $_SESSION['IS_CONNECTED'] = False;
    }
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta desc="Inscription candidat">
    <meta name="Inscription candidat" content="width=device-width">
    <title>Offre emploi | Incription candidat</title>
    <link href="css/main.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="content">
    <h1 class="titreregister">Inscription Candidat</h1>
    <?php if($_SESSION['IS_CONNECTED'] == False) : ?>
        <form class="formregister" action="inscription_user.php" method="post">
            <input class="testname" type="text" name="nom" placeholder="NOM" />
            <input class="prenom1" type="text" name="prenom" placeholder="PRENOM" />
            <input class="email" type="text" name="email" placeholder="EMAIL" />
            <input class="mdp" type="password" name="mot_de_passe" placeholder="MOT DE PASSE" />
            <input class="date" type="text" name="date_de_naissance" placeholder="DATE DE NAISSANCE" />
            <input class="tel" type="text" name="telephone" placeholder="TELEPHONE" />
            <button class="buttonindex1" type="submit">Envoyer</button>
        </form>
    <?php endif; ?>
</div>
</body>
</html>