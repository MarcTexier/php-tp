<?php
    include_once('pdo.php');



$query1 = $pdo->prepare('CREATE TABLE IF NOT EXISTS tp.offres (
    id_offre INT NOT NULL AUTO_INCREMENT,
    nom VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL,
    type VARCHAR(255) NOT NULL,
    salaire INT NOT NULL,
    PRIMARY KEY (id_offre));');
$query1->execute();

$query2 = $pdo->prepare('CREATE TABLE IF NOT EXISTS tp.candidats (
    id_candidat INT NOT NULL AUTO_INCREMENT,
    nom VARCHAR(255) NOT NULL,
    prenom VARCHAR(255) NOT NULL,
    date_naissance DATE NOT NULL,
    adresse VARCHAR(255) NOT NULL,
    addresse_mail VARCHAR(255) NOT NULL,
    telephone VARCHAR(255) NOT NULL,
    mot_de_passe VARCHAR(255) NOT NULL,
    PRIMARY KEY (id_candidat));');
$query2->execute();

$query3 = $pdo->prepare('CREATE TABLE IF NOT EXISTS tp.entreprises (
    id_entreprise INT NOT NULL AUTO_INCREMENT,
    nom VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL,
    adresse VARCHAR(255) NOT NULL,
    addresse_mail VARCHAR(255) NOT NULL,
    telephone VARCHAR(255) NOT NULL,
    mot_de_passe VARCHAR(255) NOT NULL,
    PRIMARY KEY (id_entreprise));');
$query3->execute();

$query4 = $pdo->prepare('CREATE TABLE IF NOT EXISTS tp.admins (
    id_admin INT NOT NULL AUTO_INCREMENT,
    nom VARCHAR(255) NOT NULL,
    prenom VARCHAR(255) NOT NULL,
    mot_de_passe VARCHAR(255) NOT NULL,
    PRIMARY KEY (id_admin));');
$query4->execute();

?>