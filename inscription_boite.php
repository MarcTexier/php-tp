<?php
session_start();
include_once('pdo.php');
if (empty($_POST["nom"] or empty($_POST["siret"]))) {
    header('Location: index.php');
    exit();
} else {
    $_SESSION['IS_CONNECTED'] = True;
    $_SESSION['email'] = htmlspecialchars($_POST["email"]);
    $_SESSION['nom'] = htmlspecialchars($_POST['nom']);
    $_SESSION['siret'] = htmlspecialchars($_POST['siret']);
    $telephone = htmlspecialchars($_POST['telephone']);
    $mot_passe = htmlspecialchars($_POST['mot_de_passe']);
    $date = htmlspecialchars($_POST['date_de_creation']);

    $donnee = [
        'nom' => strtolower($_SESSION['nom']),
        'siret' => strtolower($_SESSION['siret']),
        'email' => $_SESSION['email'],
        'telephone' => $telephone,
        'mot_de_passe' => $mot_passe,
        'date_creation' => $date,
    ];
    $requete = "INSERT INTO tp.entreprises (nom, siret, addresse_mail, telephone, mot_de_passe, date_creation) VALUES (:nom, :siret, :email, :telephone, :mot_de_passe, :date_creation)";
    $query1 = $pdo->prepare($requete);
    $query1->execute($donnee);
    header('Location: home_boite.php');
    exit();
}
?>