<?php
session_start();
$nom = $_SESSION["nom"];
$siret = $_SESSION["siret"];
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
    <meta desc="Login">
    <meta name="Login" content="width=device-width">
    <title>Offre emploi | login</title>
    <link href="css/main.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<div id="content-home">
		<header>
			<div id="logo"><img class="logo" src="img/unnamed.jpg" alt="image du logo"></div>
			<?php
			if ($_SESSION['IS_CONNECTED'] == False) {
			header('Location: index.php');
			exit();
			}
			echo "Bienvenue " . $nom . " " . $siret
			?>
			<a href="profil_boite.php"><button class="profil" type="submit">Mon profil</button></a>
			<button class="deco" onclick="window.location.href = 'deconnexion.php';">Déconnexion</button>
		</header>
	</div>	
</body>

</html>

<?PHP
if (!empty($_FILES['uploaded_file'])) {
	$path = './' . $nom . '_' . $siret . '/';
	if (!is_dir($path)) {
		mkdir($path, 0700);
		$path = $path . basename($_FILES['uploaded_file']['name']);
		usleep(2000000);
		if (move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $path)) {
			echo "Le fichier " .  basename($_FILES['uploaded_file']['name']) .
				" a bien été envoyé";
		} else {
			echo "Error lors de l'upload de votre fichier, réessayer!";
		}
	}
}
?>