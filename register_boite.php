<?php
    session_start();
    if (!isset($_SESSION['IS_CONNECTED'])) {
        $_SESSION['IS_CONNECTED'] = False;
    }
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta desc="Inscription entreprise">
    <meta name="Inscription entreprise" content="width=device-width">
    <title>Offre emploi | Incription entreprise</title>
    <link href="css/main.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="content">
    <script></script>
    <h1 class="titreregister">Inscription Entreprise</h1>
    <?php if($_SESSION['IS_CONNECTED'] == False) : ?>
        <form class="formregister" action="inscription_boite.php" method="post">
            <input class="testname" type="text" name="nom" placeholder="NOM" />
            <input class="prenom1" type="text" name="siret" placeholder="SIRET" />
            <input class="email" type="text" name="email" placeholder="EMAIL" />
            <input class="mdp" type="password" name="mot de passe" placeholder="MOT DE PASSE" />
            <input class="date" type="text" name="date_de_création" placeholder="DATE DE CREATION" />
            <input class="tel" type="text" name="telephone" placeholder="TELEPHONE" />
            <button class="buttonindex1" type="submit">Envoyer</button>
        </form>
    <?php endif; ?>
</div>
</body>
</html>