<?php
session_start();
include_once('pdo.php');
if (empty($_POST['nom'] or empty($_POST['siret']))) {
    header('Location: index.php');
    exit();
} else {
    $query1 = $pdo->prepare('SELECT * FROM entreprises');
        $query1->execute();
        $liste_boite = $query1->fetchAll();
        foreach ($liste_boite as $boite) {
            $nom = $boite['nom'];
            $siret = $boite['siret'];
            if (strtolower($_POST['nom']) == $nom and strtolower($_POST['siret']) == $siret) {
                $_SESSION['IS_CONNECTED'] = TRUE;
                $_SESSION['nom'] = htmlspecialchars($_POST['nom']);
                $_SESSION['siret'] = htmlspecialchars($_POST['siret']);
                header('Location: home_boite.php');
                exit();
            }
        }
        header('Location: index.php');
        exit();
}
?>