<?php
session_start();
include_once('pdo.php');
if (empty($_POST['nom'] or empty($_POST['prenom']))) {
    header('Location: index.php');
    exit();
} else {
    $query1 = $pdo->prepare('SELECT * FROM candidats');
        $query1->execute();
        $liste_candidats = $query1->fetchAll();
        foreach ($liste_candidats as $candidat) {
            $nom = $candidat['nom'];
            $prenom = $candidat['prenom'];
            if (strtolower($_POST['nom']) == $nom and strtolower($_POST['prenom']) == $prenom) {
                $_SESSION['IS_CONNECTED'] = TRUE;
                $_SESSION['nom'] = htmlspecialchars($_POST['nom']);
                $_SESSION['prenom'] = htmlspecialchars($_POST['prenom']);
                header('Location: home_user.php');
                exit();
            }
        }
        header('Location: index.php');
        exit();
}
?>