<?php
    session_start();
    if (!isset($_SESSION['IS_CONNECTED'])) {
        $_SESSION['IS_CONNECTED'] = False;
    }
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta desc="Login">
    <meta name="Login" content="width=device-width">
    <title>Offre emploi | login</title>
    <link href="css/main.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="content">
    <script></script>
    <h1 class="titreindex">Connexion</h1>
    <?php if($_SESSION['IS_CONNECTED'] == False) : ?>
        <a class="register" href="register_user.php">Inscription candidat</a>
        <a href="register_boite.php">Inscription entreprise</a>
        <form class="formindex" action="connexion.php" method="post">
            <input class="osef" type="text" name="prenom" placeholder="PRENOM" style="width: 200px; height: 50px;">
            <input class="pd" type="text" name="nom" placeholder="NOM" style="margin-left: 50px; width: 200px; height: 50px;">
            <button class="buttonindex" type="submit">Envoyer</button>
        </form>
    <?php endif; ?>
    <?php if($_SESSION['IS_CONNECTED'] == False) : ?>
        <form class="formindex" action="connexion_boite.php" method="post">
            <input class="osef" type="text" name="nom" placeholder="NOM" style="width: 200px; height: 50px;">
            <input class="pd" type="text" name="siret" placeholder="SIRET" style="margin-left: 50px; width: 200px; height: 50px;">
            <button class="buttonindex" type="submit">Envoyer</button>
        </form>
    <?php endif; ?>

    <?php
    if ($_SESSION['IS_CONNECTED'] == True) {
        header('Location: home.php');
        exit();      
    }
    ?>
</div>
</body>
</html>