<?php
session_start();
include_once('pdo.php');
if (empty($_POST['nom'] or empty($_POST['prenom']))) {
    header('Location: index.php');
    exit();
} else {
                $_SESSION['IS_CONNECTED'] = True;
                $_SESSION['email'] = htmlspecialchars($_POST["email"]);
                $_SESSION['nom'] = htmlspecialchars($_POST['nom']);
                $_SESSION['prenom'] = htmlspecialchars($_POST['prenom']);
                $telephone = htmlspecialchars($_POST['telephone']);
                $mot_passe = htmlspecialchars($_POST['mot_de_passe']);
                $date = htmlspecialchars($_POST['date_de_naissance']);
                $donnee = [
                    'nom' => strtolower($_SESSION['nom']),
                    'prenom' => strtolower($_SESSION['prenom']),
                    'email' => $_SESSION['email'],
                    'telephone' => $telephone,
                    'mot_de_passe' => $mot_passe,
                    'date_de_naissance' => $date,
                ];
                $requete = "INSERT INTO tp.candidats (nom, prenom, addresse_mail, telephone, mot_de_passe, date_naissance) VALUES (:nom, :prenom, :email, :telephone, :mot_de_passe, :date_de_naissance)";
                $query1 = $pdo->prepare($requete);
                $query1->execute($donnee);
                header('Location: home_user.php');
                exit();
}
?>