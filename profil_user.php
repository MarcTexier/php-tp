<?php
session_start();
$prenom = $_SESSION["prenom"];
$nom = $_SESSION["nom"];
?>

<!DOCTYPE html>
<html>

<head>
	<title>Home</title>
</head>

<body>
	<?php
	if ($_SESSION['IS_CONNECTED'] == False) {
		header('Location: index.php');
		exit();
	}
	echo "Bienvenue " . $prenom . " " . $nom
	?>
	<br><br>
	<button onclick="window.location.href = 'deconnexion.php';">Déconnexion</button>
	<br>
	<br>
	<form enctype="multipart/form-data" action="home.php" method="post">
		<p>Upload your file</p>
		<input type="file" name="uploaded_file"></input><br />
		<input type="submit" value="Upload"></input>
	</form>
</body>

</html>

<?PHP
if (!empty($_FILES['uploaded_file'])) {
	$path = './' . $prenom . '_' . $nom . '/';
	if (!is_dir($path)) {
		mkdir($path, 0700);
		$path = $path . basename($_FILES['uploaded_file']['name']);
		usleep(2000000);
		if (move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $path)) {
			echo "Le fichier " .  basename($_FILES['uploaded_file']['name']) .
				" a bien été envoyé";
		} else {
			echo "Error lors de l'upload de votre fichier, réessayer!";
		}
	}
}
?>