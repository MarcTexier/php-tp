<?php
session_start();
$prenom = $_SESSION["prenom"];
$nom = $_SESSION["nom"];
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
    <meta desc="Page d'acceuil">
    <meta name="Page d'acceuil" content="width=device-width">
    <title>Offre emploi | Home</title>
    <link href="css/main.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<div id="content-home">
		<header>
			<div id="logo"><img class="logo" src="img/unnamed.jpg" alt="image du logo"></div>
			<?php
			if ($_SESSION['IS_CONNECTED'] == False) {
			header('Location: index.php');
			exit();
			}
			echo "Bienvenue " . $prenom . " " . $nom
			?>
			<a href="profil_user.php"><button class="profil" type="submit">Mon profil</button></a>
			<button class="deco" onclick="window.location.href = 'deconnexion.php';">Déconnexion</button>
		</header>
	</div>	
</body>

</html>
